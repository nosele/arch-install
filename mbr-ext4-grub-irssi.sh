#!/usr/bin/env bash

# Partition, create & mount filesystems
echo -e "o\nn\np\n1\n\n+256M\nn\np\n2\n\n+21G\nn\np\n3\n\n\nw" | fdisk /dev/sda

mkfs.ext2 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3

mount /dev/sda2 /mnt
mkdir /mnt/{home,boot}
mount /dev/sda1 /mnt/boot
mount /dev/sda3 /mnt/home

#Software
wget https://bitbucket.org/nosele/arch-install/downloads/mirrorlist
##ISO mirrorlist
cp ./mirrorlist /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel grub perl-libwww irssi

#Booting
genfstab -p /mnt >> /mnt/etc/fstab

arch-chroot /mnt grub-install --target=i386-pc --recheck --debug /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt mkinitcpio -p linux

#Configuration
#line 161 /etc/locale.gen
echo "en_US.UTF-8 UTF-8" > /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
#unset only when need is verified
#arch-chroot /mnt locale > /etc/locale.conf

echo "arch" > /mnt/etc/hostname
arch-chroot /mnt systemctl enable dhcpcd.service

arch-chroot ln -s /usr/share/zoneinfo/Canada/Eastern /etc/localtime
